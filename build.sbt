// Turn this project into a Scala.js project by importing these settings

import sbt.Keys.{mainClass, resources, _}
import sbtassembly.Plugin.AssemblyKeys.{jarName, _}
import spray.revolver.RevolverPlugin.Revolver

enablePlugins(WorkbenchPlugin)

scalaVersion := "2.11.8"

val ReportConstructor = crossProject.settings(
  scalaVersion := "2.11.8",
  version := "0.1-SNAPSHOT",
  libraryDependencies ++= Seq(
    "com.lihaoyi" %%% "upickle" % "0.4.3",
    "com.lihaoyi" %%% "autowire" % "0.2.6",
    "com.lihaoyi" %%% "scalatags" % "0.6.1"
  ),
  addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full),
  libraryDependencies ++= Seq(
    "io.circe" %%% "circe-core",
    "io.circe" %%% "circe-generic",
    "io.circe" %%% "circe-parser"
  ).map(_ % "0.7.0")

).jsSettings(
  name := "Client",
  libraryDependencies ++= Seq(
    "org.scala-js" %%% "scalajs-dom" % "0.9.1",
    "be.doeraene" %%% "scalajs-jquery" % "0.9.1"
  ),

  skip in packageJSDependencies := false,
  jsDependencies += "org.webjars" % "jquery" % "2.1.4" / "2.1.4/jquery.js",
  persistLauncher := true
).jvmSettings(
  Revolver.settings: _*)
  .jvmSettings(
    resolvers += "Atlassian 3rd-Party Repository" at "https://maven.atlassian.com/3rdparty/",
    name := "Server",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-http-core" % "10.0.5",
      "com.typesafe.akka" %% "akka-http" % "10.0.5",
      "org.scalikejdbc" %% "scalikejdbc" % "2.5.1",
      "org.scalikejdbc" %% "scalikejdbc-config" % "2.5.1",
      "ch.qos.logback" % "logback-classic" % "1.2.1",
      "com.oracle" % "ojdbc6" % "12.1.0.1-atlassian-hosted",
      "com.typesafe.play" %% "anorm" % "2.5.1",
      "org.scalatest" % "scalatest_2.11" % "3.0.1" % "test"
    ),
    assemblySettings,
    mainClass in assembly := Some("ReportConstructor.Server"),
    jarName in assembly := "ReportConstructor.jar"
  )

val ReportConstructorJS = ReportConstructor.js
val ReportConstructorJVM = ReportConstructor.jvm.settings(
  resources in Compile ++= Seq({
    (fastOptJS in(ReportConstructorJS, Compile)).value
    (artifactPath in(ReportConstructorJS, Compile, fastOptJS)).value
  }, {
    (packageScalaJSLauncher in(ReportConstructorJS, Compile)).value
    (artifactPath in(ReportConstructorJS, Compile, packageScalaJSLauncher)).value
  })
)

logLevel := Level.Debug