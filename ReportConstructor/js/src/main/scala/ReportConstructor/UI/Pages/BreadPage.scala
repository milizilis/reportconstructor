package ReportConstructor.UI.Pages

import ReportConstructor.Utils._
import org.scalajs.dom.html.{Div, LI}

import scalatags.JsDom._
import scalatags.JsDom.all._

/**
  * Created by Denis on 17.03.2017.
  */

trait BreadPage {
  // Название страницы, которое будет в breadcrumbs
  def getName: String

  // Родительская страница
  def getParent: Option[BreadPage]

  // Действие при нажатии на название страницы в breadcrumbs. Убирается у активной страницы
  def getOpenAction: () => Unit = () => RedirectTo(this)

  // ID для узла в breadcrumbs. Нужен для установки активной страницы
  def getNavNodeId: String

  // Действия по наполнению контента. Метод может быть пустым
  def initialize(): Unit

  // Динамическая разметка контента в scalatags
  val content: TypedTag[Div]

  // Узел в breadcrumbs
  val navigationNode: TypedTag[LI] = li(id := getNavNodeId, a(href := "#", onclick := getOpenAction)(getName))
}
