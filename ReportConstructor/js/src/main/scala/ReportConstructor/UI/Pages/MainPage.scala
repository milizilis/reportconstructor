package ReportConstructor.UI.Pages

import ReportConstructor.Utils
import org.scalajs.dom.html.Div

import scalatags.JsDom.TypedTag
import scalatags.JsDom.all._

/**
  * Created by Denis on 08.04.2017.
  */

object MainPage extends BreadPage {
  override def getName = "Главная"

  override def getParent = None

  override def getNavNodeId: String = "mainPage"

  override def initialize(): Unit = {}

  override val content: TypedTag[Div] =
    div(
      paddingLeft := "15px",
      h3("Главная страница"),
      div(
        `class` := "col-md-6 btn-group-vertical",
        button(
          `type` := "button",
          `class` := "btn btn-default",
          width := "250px",
          height := "40px",
          title := "Перейти к расчетным показателям",
          onclick := { () => Utils.RedirectTo(UserIndicesPage) }
        )("Перейти к расчетным показателям"),
        button(
          paddingLeft := "10px",
          `type` := "button",
          `class` := "btn btn-default",
          width := "250px",
          height := "40px",
          title := "Перейти к списку отчетов",
          onclick := { () => Utils.RedirectTo(ReportsListPage) }
        )("Перейти к списку отчетов")
      )
    )
}