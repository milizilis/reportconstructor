package ReportConstructor.UI.Pages

import ReportConstructor.Model.{RegionInfo, ReportInfo, reportDetails}
import ReportConstructor.UI.CommonElements
import ReportConstructor._
import autowire._
import org.scalajs.dom.html.{Anchor, Div, Input}
import org.scalajs.dom.{DragEvent, Event}
import org.scalajs.jquery.jQuery

import scala.collection.mutable.ListBuffer
import scala.scalajs.concurrent.JSExecutionContext.Implicits.runNow
import scala.scalajs.js
import scalatags.JsDom.TypedTag
import scalatags.JsDom.all._

/**
  * Created by Denis on 10.04.2017.
  */

object ReportsEditingPage extends BreadPage {
  lazy val indexList = new CommonElements.IndicesList(showActions = false)

  override def getName: String = "Создание и редактирование отчетов"

  override def getParent: Option[BreadPage] = Some(ReportsListPage)

  override def getNavNodeId: String = "reportsEditingPage"

  override def initialize(): Unit = {
    indexList.Init()
    fillRegions()
    initSelects()
    Local.resetEditedElements()
    resetLocalVariables()
  }

  override val content: TypedTag[Div] =
    div(
      h3(paddingLeft := "15px")("Создание и редактирование отчетов"),
      div(
        `class` := "container-fluid",
        paddingLeft := "15px",
        div(
          `class` := "row",
          form(
            div(
              `class` := "col-md-12",
              paddingBottom := "10px",
              input(
                id := "inputReportName",
                `class` := "form-control",
                required,
                placeholder := "Наименование отчета"
              )
            ),
            div(
              `class` := "col-md-6",
              indexList.content // Список показателей
            ),
            div(
              `class` := "col-md-6",
              label("Список показателей для отображения"),
              div(
                id := "divReportIndices",
                `class` := "divReportIndices",
                contenteditable := true,
                ondrop := { (event: DragEvent) => Utils.drop(event, "divReportIndices", allowRepeatedDrop = false) },
                ondragover := {
                  Utils.allowDrop(_: DragEvent)
                },
                placeholder := "Список показателей для отображения"
              ),
              div(
                `class` := "row",
                div(
                  paddingTop := "10px",
                  `class` := "col-md-5",
                  label("Субъекты"),
                  ul(
                    id := "ulRegionsSource",
                    maxHeight := "260px",
                    overflowY := "scroll",
                    `class` := "list-group"
                  )
                ),
                div(
                  paddingTop := "110px",
                  `class` := "col-md-2 btn-group-vertical",
                  button(`class` := "btn btn-default", onclick := { (event: Event) => event.preventDefault(); addSelectedRegion() })(">"),
                  button(`class` := "btn btn-default", onclick := { (event: Event) => event.preventDefault(); removeSelectedRegion() })("<")
                ),
                div(
                  paddingTop := "10px",
                  `class` := "col-md-5",
                  label("Список субъектов для отображения"),
                  label(
                    id := "lblEmptyRegionsList",
                    color := "gray",
                    paddingTop := "100px",
                    paddingLeft := "35px"
                  )("Список субъектов пуст"),
                  ul(
                    id := "ulRegionsForView",
                    maxHeight := "260px",
                    overflowY := "auto",
                    `class` := "list-group"
                  )
                )
              ),
              div(
                `class` := "row",
                div(
                  `class` := "col-md-4",
                  label("Начало интервала"),
                  select(id := "selectYearsFrom", `class` := "form-control"),
                  select(id := "selectQuartersFrom", `class` := "form-control", marginTop := "15px")
                ),
                div(
                  `class` := "col-md-4",
                  label("Конец интервала"),
                  select(id := "selectYearsTo", `class` := "form-control"),
                  select(id := "selectQuartersTo", `class` := "form-control", marginTop := "15px")
                ),
                div(
                  `class` := "col-md-4",
                  label("Тип отображения"),
                  select(id := "selectViewType", `class` := "form-control"),
                  input(
                    `type` := "checkbox",
                    id := "cbConsiderYears",
                    width := "20px",
                    height := "20px",
                    marginTop := "20px"
                  ),
                  label(marginTop := "5px", marginLeft := "10px")("Учитывать кварталы")
                ),
                div(
                  float := "right",
                  paddingTop := "25px",
                  paddingRight := "15px",
                  button(
                    `type` := "button",
                    `class` := "btn btn-default",
                    width := "120px",
                    height := "40px",
                    title := "Отменить создание/редактирование отчета и очистить данные",
                    onclick := { () => cancelReportEditing() },
                    marginRight := "10px"
                  )("Отмена"),
                  button(
                    `type` := "submit",
                    `class` := "btn btn-default",
                    width := "120px",
                    height := "40px",
                    title := "Сохранить отчет"
                  )("Сохранить")
                )
              )
            ),
            onsubmit := {
              (event: Event) => {
                event.preventDefault()
                saveReport()
              }
            }
          )
        )
      )
    )

  var addedRegionKeys: List[String] = Nil
  var selectedRegion: Option[RegionInfo] = None

  // Для корректной работы при переходе по breadcrumbs
  def resetLocalVariables(): Unit = {
    addedRegionKeys = Nil
    selectedRegion = None
  }

  def saveReport(): Unit = {
    val indicesList = "{.*?}".r.findAllMatchIn(Utils.getFormulaFromDiv("divReportIndices")).map(_.toString().replaceAll("{|}", "")).toList
    var errors = new ListBuffer[String]()

    if (indicesList.isEmpty) errors += "Список показателей для отображения пуст"
    if (addedRegionKeys.isEmpty) errors += "Список регионов для отображения пуст"

    if (errors.nonEmpty) js.Dynamic.global.alert(errors.mkString("\n"))
    else {
      Local.editedReport.fold(insertReport(indicesList))(updateReport(_, indicesList))
      clearReportInputGroup()
    }
  }

  def insertReport(indexKeysList: List[String]): Unit = {
    val details = getReportDetails(indexKeysList)
    val insertedReport = ReportInfo(
      key = scala.util.Random.nextInt(10000).toString,
      jQuery("#inputReportName").value().toString,
      details
    )

    Client[ServerAPI].insertReport(insertedReport).call().map(_ => {
      Local.reloadCachedReports()
      js.Dynamic.global.alert("Новый отчет создан")
      Utils.RedirectTo(ReportsListPage)
    })
  }

  def getReportDetails(indexKeysList: List[String]): reportDetails = {
    val rangeFrom = jQuery("#selectYearsFrom option:selected").value().toString.toInt -> jQuery("#selectQuartersFrom option:selected").value().toString.toInt
    val rangeTo = jQuery("#selectYearsTo option:selected").value().toString.toInt -> jQuery("#selectQuartersTo option:selected").value().toString.toInt

    reportDetails(
      indexKeysList,
      addedRegionKeys,
      rangeFrom,
      rangeTo,
      jQuery("#selectViewType option:selected").value().toString,
      jQuery("#cbConsiderYears").prop("checked").toString.toBoolean
    )
  }

  def startEditReport(reportKey: String): Unit = {
    Local.getReportByKey(reportKey).map(editableReport => {
      editableReport.fold()(report => {

        def addIndex(index: TypedTag[Input]) = jQuery("#divReportIndices").append(index.render)

        report.details.indexKeysList.foreach(Local.getIndexByKey(_).map(index => addIndex(Utils.getTagByIndex(index))))
        report.details.regionKeysList.foreach(key => {
          Local.getRegionByKey(key).map(region => {
            selectedRegion = region
            addSelectedRegion()
          })
        })

        jQuery("#selectYearsFrom").`val`(report.details.rangeFromYQ._1.toString)
        jQuery("#selectQuartersFrom").`val`(report.details.rangeFromYQ._2.toString)

        jQuery("#selectYearsTo").`val`(report.details.rangeToYQ._1.toString)
        jQuery("#selectQuartersTo").`val`(report.details.rangeToYQ._2.toString)

        jQuery("#selectViewType").`val`(report.details.diagramType.toString)
        jQuery("#inputReportName").value(report.reportName)
        jQuery("#cbConsiderYears").prop("checked", report.details.considerQuarters)
      })

      Local.editedReport = editableReport
    })
  }

  def updateReport(updatedReport: ReportInfo, indicesList: List[String]): Unit = {
    Client[ServerAPI].updateReport(updatedReport.copy(
      reportName = jQuery("#inputReportName").value().toString,
      details = getReportDetails(indicesList)
    )).call().map(_ => {
      Local.reloadCachedReports()
      Local.editedReport = None
      js.Dynamic.global.alert("Расчетный показатель изменен")
      Utils.RedirectTo(ReportsListPage)
    })
  }

  def cancelReportEditing(): Unit = {
    Local.editedReport.fold()(_ => Local.editedReport = None)
    clearReportInputGroup()
  }

  def setDefaultRange(): Unit = {
    jQuery("#selectYearsFrom").`val`("2010")
    jQuery("#selectYearsTo").`val`("2015")
    jQuery("#selectQuartersFrom, #selectQuartersTo").`val`("4")
    jQuery("#selectViewType").`val`("bar")
  }

  def clearReportInputGroup(): Unit = {
    jQuery("#divReportIndices, #ulRegionsForView").empty()
    addedRegionKeys = Nil
    jQuery("#lblEmptyRegionsList").show()
    jQuery("#inputReportName").value("")
    setDefaultRange()
    jQuery("#considerYears").prop("checked", false)
  }

  def fillRegions(): Unit = {
    val ulRegionsSource = jQuery("#ulRegionsSource")
    Local.getCachedRegions.foreach(allRegions => allRegions.map(region => getTagByRegion(Some(region))).foreach(li => ulRegionsSource.append(li.render)))
  }

  def getTagByRegion(region: Option[RegionInfo]): TypedTag[Anchor] =
    a(
      id := region.fold("-1")(_.key),
      href := "#",
      `class` := "list-group-item",
      onclick := { () => selectedRegion = region }
    )(region.fold("Регион не существует")(_.name))

  def addSelectedRegion(): Unit = {
    selectedRegion.fold[Unit]()(region => {
      if (!addedRegionKeys.contains(region.key)) {
        jQuery("#ulRegionsForView").append(getTagByRegion(Some(region)).render)
        addedRegionKeys = addedRegionKeys :+ region.key
      }
    })

    selectedRegion = None
    if (addedRegionKeys.nonEmpty) jQuery("#lblEmptyRegionsList").hide()
  }

  def removeSelectedRegion(): Unit = {
    selectedRegion.fold[Unit]()(region => {
      jQuery("#ulRegionsForView #" + region.key).remove
      addedRegionKeys = addedRegionKeys.filterNot(_ == region.key)
    })

    selectedRegion = None
    if (addedRegionKeys.isEmpty) jQuery("#lblEmptyRegionsList").show()
  }

  def initSelects(): Unit = {
    Local.yearsRange.foreach(year => jQuery("#selectYearsFrom, #selectYearsTo").append(option(value := year)(year).render))
    Local.quartersRange.foreach(quarter => jQuery("#selectQuartersFrom, #selectQuartersTo").append(option(value := quarter._2)(quarter._1).render))

    (("Гистограмма" -> "bar") :: Nil).foreach(viewType =>
      jQuery("#selectViewType").append(option(value := viewType._2)(viewType._1).render))

    setDefaultRange()
  }
}
