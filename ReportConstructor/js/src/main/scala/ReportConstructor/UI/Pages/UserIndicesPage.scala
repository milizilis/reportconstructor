package ReportConstructor.UI.Pages

import ReportConstructor.Model.IndexInfo
import ReportConstructor.UI.CommonElements
import ReportConstructor._
import autowire._
import org.scalajs.dom.html.Div
import org.scalajs.dom.{DragEvent, Event}
import org.scalajs.jquery.jQuery

import scala.scalajs.concurrent.JSExecutionContext.Implicits.runNow
import scala.scalajs.js
import scalatags.JsDom._
import scalatags.JsDom.all._

/**
  * Created by Denis on 08.04.2017.
  */

object UserIndicesPage extends BreadPage {
  lazy val indexList = new CommonElements.IndicesList(showActions = true)

  override def getName = "Расчетные показатели"

  override def getParent = Some(MainPage)

  override def getNavNodeId: String = "userIndicesPage"

  override def initialize(): Unit = {
    indexList.Init()
    Local.resetEditedElements()
  }

  override val content: TypedTag[Div] =
    div(
      h3(paddingLeft := "15px")("Создание и редактирование расчетных показателей"),
      div(
        `class` := "container-fluid",
        paddingLeft := "15px",
        div(
          `class` := "row",
          div(
            `class` := "col-md-6",
            indexList.content // Список показателей
          ),
          div(
            `class` := "col-md-6",
            form(
              label("Редактирование расчетного показателя"),
              div(
                paddingBottom := "44px",
                input(
                  id := "inputIndexFullName",
                  `class` := "form-control",
                  required,
                  float := "left",
                  width := "79%",
                  placeholder := "Наименование расчетного показателя"
                ),
                input(
                  id := "inputIndexCode",
                  `class` := "form-control",
                  required,
                  float := "right",
                  width := "20%",
                  placeholder := "Код"
                )
              ),
              div(
                id := "divUserIndex",
                `class` := "divUserIndex",
                contenteditable := true,
                ondrop := { (event: DragEvent) => Utils.drop(event, "divUserIndex") },
                ondragover := {
                  Utils.allowDrop(_: DragEvent)
                }
              ),
              div(
                `class` := "btn-group",
                paddingTop := "20px",
                button(`type` := "button", `class` := "btn btn-default btnOperation", onclick := { () => addOperationSymbol("+") })("+"),
                button(`type` := "button", `class` := "btn btn-default btnOperation", onclick := { () => addOperationSymbol("-") })("-"),
                button(`type` := "button", `class` := "btn btn-default btnOperation", onclick := { () => addOperationSymbol("*") })("*"),
                button(`type` := "button", `class` := "btn btn-default btnOperation", onclick := { () => addOperationSymbol("/") })("/"),
                button(`type` := "button", `class` := "btn btn-default btnOperation", onclick := { () => addOperationSymbol("(") })("("),
                button(`type` := "button", `class` := "btn btn-default btnOperation", onclick := { () => addOperationSymbol(")") })(")")
              ),
              div(
                float := "right",
                paddingTop := "20px",
                button(
                  id := "btnCancelUserIndex",
                  `type` := "button",
                  `class` := "btn btn-default",
                  width := "120px",
                  height := "40px",
                  title := "Отменить создание/редактирование показателя и очистить данные",
                  onclick := { () => cancelIndexEditing() },
                  marginRight := "10px"
                )("Отмена"),
                button(
                  id := "btnSaveUserIndex",
                  `type` := "submit",
                  `class` := "btn btn-default",
                  width := "120px",
                  height := "40px",
                  title := "Сохранить показатель"
                )("Сохранить")
              ),
              onsubmit := {
                (event: Event) => {
                  event.preventDefault()
                  saveUserIndex()
                }
              }
            )
          )
        )
      )
    )

  def startEditUserIndex(indexKey: String): Unit = {
    val divUserIndex = jQuery("#divUserIndex")
    val inputIndexFullName = jQuery("#inputIndexFullName")
    val inputIndexCode = jQuery("#inputIndexCode")

    if (divUserIndex.html().trim == "" || js.Dynamic.global.confirm("Поле редактирования будет очищено. Продолжить?").asInstanceOf[Boolean]) {
      divUserIndex.empty

      Local.getIndexByKey(indexKey).map(editableUserIndex => {

        editableUserIndex.fold()(index => {
          Utils.setDivFromFormula(index.formula, "divUserIndex")
          inputIndexFullName.value(index.fullName)
          inputIndexCode.value(index.code)
        })

        Local.editedIndex = editableUserIndex
      })
    }
  }

  def insertUserIndex(formula: String): Unit = {
    val insertedIndexCode = jQuery("#inputIndexCode").value().toString
    val insertedIndexFullName = jQuery("#inputIndexFullName").value().toString

    val insertedIndex =
      IndexInfo(
        code = insertedIndexCode,
        groupKey = NoDBIndexGroupKeys.ig_Usr,
        shortName = "РП: " + insertedIndexCode,
        fullName = insertedIndexFullName,
        parentKey = "",
        isLeaf = false,
        isUser = true,
        formula = formula)

    Client[ServerAPI].insertIndex(insertedIndex).call().map(_ => {
      Local.reloadCachedIndices()
      indexList.fillIndicesTableBySelection()
      js.Dynamic.global.alert("Новый расчетный показатель создан")
    }).onFailure { case e => js.Dynamic.global.alert(e.getMessage) }
  }

  def updateUserIndex(updatedIndex: IndexInfo, newFormula: String): Unit = {
    Client[ServerAPI].updateIndex(updatedIndex.copy(
      formula = newFormula,
      code = jQuery("#inputIndexCode").value().toString,
      shortName = "РП: " + jQuery("#inputIndexCode").value().toString,
      fullName = jQuery("#inputIndexFullName").value().toString
    )).call().map(_ => {
      Local.reloadCachedIndices()
      indexList.fillIndicesTableBySelection()
      Local.editedIndex = None
      js.Dynamic.global.alert("Расчетный показатель изменен")
    })
  }

  def deleteUserIndex(deletedIndexKey: String): Unit = {
    if (js.Dynamic.global.confirm("Вы действительно хотите удалить расчетный показатель?").asInstanceOf[Boolean]) {
      Client[ServerAPI].deleteIndex(deletedIndexKey).call().map(_ => {
        Local.reloadCachedIndices()
        Local.editedIndex.fold()(index =>
          if (index.key == deletedIndexKey) Local.editedIndex = None
          else Utils.setDivFromFormula(index.formula, "divUserIndex"))
        indexList.fillIndicesTableBySelection()
      })
    }
  }

  def saveUserIndex(): Unit = {
    val formula = Utils.getFormulaFromDiv("divUserIndex")
    if (formula.trim == "") js.Dynamic.global.alert("Формула не задана")
    else {
      Local.editedIndex.fold(insertUserIndex(formula))(updateUserIndex(_, formula))
      clearUserIndexInputGroup()
    }
  }

  def clearUserIndexInputGroup(): Unit = {
    jQuery("#divUserIndex").empty
    jQuery("#inputIndexFullName, #inputIndexCode").value("")
  }

  def cancelIndexEditing(): Unit = {
    Local.editedIndex.fold()(_ => Local.editedIndex = None)
    clearUserIndexInputGroup()
  }

  def addOperationSymbol(symbol: String): Unit = jQuery("#divUserIndex").html(jQuery("#divUserIndex").html() + symbol)
}
