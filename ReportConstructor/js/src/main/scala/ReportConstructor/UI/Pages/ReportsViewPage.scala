package ReportConstructor.UI.Pages

import ReportConstructor.Model.{ReportInfo, SeriesElement}
import ReportConstructor.{Client, ServerAPI}
import autowire._
import io.circe.generic.auto._
import io.circe.syntax._
import org.scalajs.dom.html.Div

import scala.scalajs.concurrent.JSExecutionContext.Implicits.runNow
import scala.scalajs.js
import scalatags.JsDom
import scalatags.JsDom.all._

/**
  * Created by Denis on 14.04.2017.
  */

object ReportsViewPage extends BreadPage {
  override def getName: String = "Отображение отчета"

  override def getParent: Option[BreadPage] = Some(ReportsListPage)

  override def getNavNodeId: String = "reportsViewPage"

  override def initialize(): Unit = {}

  override val content: JsDom.TypedTag[Div] =
    div(
      h3(id := "h3Header", paddingLeft := "15px"),
      div(
        `class` := "container-fluid",
        paddingLeft := "15px",
        div(
          `class` := "row",
          div(id := "divReportView", `class` := "col-md-12", height := "750px", paddingBottom := "15px"),
          div(id := "cbShowDataOnDiagram", width := "350px", display := "block", marginLeft := "auto", marginRight := "auto")
        )
      )
    )

  case class reportDiagramInfo(diagramName: String, diagramType: String, series: List[SeriesElement])

  def buildReportView(report: ReportInfo): Unit = {
    Client[ServerAPI].getReportSeries(report.details).call().foreach(seriesList => {
      js.Dynamic.global.drawChart(
        reportDiagramInfo(
          diagramName = report.reportName,
          diagramType = report.details.diagramType,
          series = seriesList
        ).asJson.noSpaces)
    })
  }
}
