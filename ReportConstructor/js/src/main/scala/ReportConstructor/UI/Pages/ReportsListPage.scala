package ReportConstructor.UI.Pages

import ReportConstructor.{Client, Local, ServerAPI, Utils}
import autowire._
import org.scalajs.dom.html.Div
import org.scalajs.jquery.jQuery

import scala.scalajs.concurrent.JSExecutionContext.Implicits.runNow
import scala.scalajs.js
import scalatags.JsDom._
import scalatags.JsDom.all._

/**
  * Created by Denis on 09.04.2017.
  */

object ReportsListPage extends BreadPage {
  override def getName: String = "Список отчетов"

  override def getParent: Option[BreadPage] = Some(MainPage)

  override def getNavNodeId: String = "reportsListPage"

  override def initialize(): Unit = fillReports()

  override val content: TypedTag[Div] =
    div(
      paddingLeft := "15px",
      paddingRight := "15px",
      h3("Список отчетов"),
      div(
        overflowY := "auto",
        height := "550px",
        borderBottom := "1px solid lightgray",
        borderTop := "1px solid lightgray",
        table(`class` := "table table-bordered table-striped", tbody(id := "tableReports-body"))
      ),
      div(
        float := "right",
        paddingTop := "20px",
        button(
          `type` := "button",
          `class` := "btn btn-default",
          width := "220px",
          height := "40px",
          title := "Перейти к странице создания и редактирования отчетов",
          onclick := { () => Utils.RedirectTo(ReportsEditingPage) }
        )("Создать новый отчет")
      )
    )

  def fillReports(): Unit = {
    Local.getCachedReports.foreach(allReports => {
      val tableReports = jQuery("#tableReports-body")
      tableReports.empty()

      allReports.map(report =>
        tr(
          td(
            `class` := "col-xs-11",
            onclick := { () => Utils.RedirectTo(ReportsViewPage); ReportsViewPage.buildReportView(report) }
          )(report.reportName),
          td(
            `class` := "col-xs-2",
            div(
              paddingTop := "5px",
              `class` := "divReportButtonGroup",
              button(
                `class` := "btn btn-default",
                img(src := "img/edit-26.png"),
                title := "Редактировать отчет",
                onclick := { () => Utils.RedirectTo(ReportsEditingPage); ReportsEditingPage.startEditReport(report.key) }
              ),
              button(
                `class` := "btn btn-default",
                img(src := "img/trash-26.png"),
                title := "Удалить отчет",
                onclick := { () => deleteReport(report.key) }
              )
            )
          )
        ).render
      ).foreach(tableReports.append(_))
    })
  }

  def deleteReport(key: String): Unit = {
    if (js.Dynamic.global.confirm("Вы действительно хотите удалить отчет?").asInstanceOf[Boolean]) {
      Client[ServerAPI].deleteReport(key).call().map(_ => {
        Local.reloadCachedReports()
        fillReports()
      })
    }
  }
}
