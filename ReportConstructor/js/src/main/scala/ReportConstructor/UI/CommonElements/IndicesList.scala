package ReportConstructor.UI.CommonElements

import ReportConstructor.UI.Pages.UserIndicesPage
import ReportConstructor.Utils.drag
import ReportConstructor.{NoDBIndexGroupKeys, Local}
import org.scalajs.dom.DragEvent
import org.scalajs.dom.html.Div
import org.scalajs.jquery.jQuery

import scala.scalajs.concurrent.JSExecutionContext.Implicits.runNow
import scalatags.JsDom._
import scalatags.JsDom.all._

/**
  * Created by Denis on 10.04.2017.
  */

class IndicesList(showActions: Boolean) {
  def content: TypedTag[Div] =
    div(
      label("Показатели"),
      div(
        paddingBottom := "10px",
        select(id := "selectIndexGroup", `class` := "form-control", onchange := { () => fillIndicesTableBySelection() })
      ),
      div(
        overflowY := "auto",
        height := "550px",
        borderBottom := "1px solid lightgray",
        borderTop := "1px solid lightgray",
        table(`class` := "table table-bordered table-striped", tbody(id := "tableIndices-body"))
      )
    )

  def Init(): Unit = {
    // Загружаем кэш для индексов и групп индексов, только затем заполняем контент.
    // В противном случае иконки индексов могут иметь неправильную окраску при первом открытии страницы

    val futureIndexGroup = Local.getCachedIndexGroup
    val futureIndices = Local.getCachedIndices

    for {
      _ <- futureIndexGroup
      _ <- futureIndices
    } {
      fillIndexGroups()
      fillIndicesTable()
    }
  }

  def fillIndicesTable(groupKey: Option[String] = None): Unit = {
    Local.getCachedIndices.foreach(allIndices => {
      val tableIndices = jQuery("#tableIndices-body")
      tableIndices.empty()

      val indices = groupKey match {
        case Some(NoDBIndexGroupKeys.ig_All) | None => allIndices
        case Some(key) => allIndices.filter(_.groupKey == key)
      }

      val toAdd = indices.map(index =>
        tr(
          td(`class` := "col-xs-5")(index.fullName), {
            if (showActions)
              td(
                `class` := "col-xs-1", {
                  if (index.groupKey == NoDBIndexGroupKeys.ig_Usr)
                    div(
                      paddingTop := "15px",
                      `class` := "divUserIndexButtonGroup",
                      button(
                        `class` := "btn btn-default",
                        img(src := "img/edit-26.png"),
                        title := "Редактировать расчетный показатель",
                        onclick := { () => UserIndicesPage.startEditUserIndex(index.key) }
                      ),
                      button(
                        `class` := "btn btn-default",
                        img(src := "img/trash-26.png"),
                        title := "Удалить расчетный показатель",
                        onclick := { () => UserIndicesPage.deleteUserIndex(index.key) }
                      )
                    )
                  else p("Первичный показатель")
                }
              )
          },
          td(
            `class` := "col-xs-2",
            div(
              draggable := true,
              `class` := "divIndexIcon",
              borderColor := {
                index.groupKey match {
                  case NoDBIndexGroupKeys.ig_Usr => "orange"
                  case primaryIndexGroupKey =>
                    var borderColor = ""
                    Local.getIndexGroupByKey(primaryIndexGroupKey).map(indexGroup => borderColor = indexGroup.fold("black")(_.iconDetails.borderColor))
                    borderColor
                }
              },
              contenteditable := false,
              ondragstart := { e: DragEvent => drag(e) },
              wordWrap := "break-word",
              id := index.key
            )(index.shortName)
          )
        ).render
      )

      toAdd.foreach(i => tableIndices.append(i))
    })
  }

  def fillIndexGroups(): Unit = {
    val selectIndexGroup = jQuery("#selectIndexGroup")

    Local.getCachedIndexGroup.foreach(indexGroups => {
      selectIndexGroup.append(option(value := NoDBIndexGroupKeys.ig_All)("Все").render)

      indexGroups.foreach(i =>
        selectIndexGroup.append(
          option(value := i.key)(i.fullName).render))

      selectIndexGroup.append(option(value := NoDBIndexGroupKeys.ig_Usr)("Расчетные показатели").render)
    })
  }

  def fillIndicesTableBySelection(): Unit = fillIndicesTable(Option(jQuery("#selectIndexGroup option:selected").value().toString))
}
