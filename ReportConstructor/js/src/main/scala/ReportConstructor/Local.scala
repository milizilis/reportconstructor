package ReportConstructor

import ReportConstructor.Model._
import autowire._

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.runNow

/**
  * Created by Denis on 10.04.2017.
  */

object Local {
  val yearsRange: List[Int] = (2005 to 2030).toList
  val quartersRange: List[(String, Int)] = ("Январь - Март" -> 1) :: ("Январь - Июнь" -> 2) :: ("Январь - сентябрь" -> 3) :: ("Январь - декабрь" -> 4) :: Nil

  private var cachedIndices: Option[Future[List[IndexInfo]]] = None
  private var cachedReports: Option[Future[List[ReportInfo]]] = None
  private var cachedRegions: Option[Future[List[RegionInfo]]] = None
  private var cachedIndexGroups: Option[Future[List[IndexGroup]]] = None

  def getCachedIndices: Future[List[IndexInfo]] = cachedIndices.getOrElse(loadAndGetCachedIndices())

  def getCachedReports: Future[List[ReportInfo]] = cachedReports.getOrElse(loadAndGetCachedReports())

  def getCachedRegions: Future[List[RegionInfo]] = cachedRegions.getOrElse(loadAndGetCachedRegions())

  def getCachedIndexGroup: Future[List[IndexGroup]] = cachedIndexGroups.getOrElse(loadAndGetCachedIndexGroups())

  private def loadAndGetCachedIndices(): Future[List[IndexInfo]] = {
    reloadCachedIndices()
    cachedIndices.get
  }

  private def loadAndGetCachedReports(): Future[List[ReportInfo]] = {
    reloadCachedReports()
    cachedReports.get
  }

  private def loadAndGetCachedRegions(): Future[List[RegionInfo]] = {
    cachedRegions = Some(Client[ServerAPI].loadRegions().call())
    cachedRegions.get
  }

  private def loadAndGetCachedIndexGroups(): Future[List[IndexGroup]] = {
    cachedIndexGroups = Some(Client[ServerAPI].loadIndexGroups().call())
    cachedIndexGroups.get
  }

  def reloadCachedIndices(): Unit = cachedIndices = Some(Client[ServerAPI].loadIndices().call())

  def reloadCachedReports(): Unit = cachedReports = Some(Client[ServerAPI].loadReports().call())

  def getIndexByKey(key: String): Future[Option[IndexInfo]] = getCachedIndices.map(_.find(_.key == key))

  def getReportByKey(key: String): Future[Option[ReportInfo]] = getCachedReports.map(_.find(_.key == key))

  def getRegionByKey(key: String): Future[Option[RegionInfo]] = getCachedRegions.map(_.find(_.key == key))

  def getIndexGroupByKey(key: String): Future[Option[IndexGroup]] = getCachedIndexGroup.map(_.find(_.key == key))

  var editedIndex: Option[IndexInfo] = None
  var editedReport: Option[ReportInfo] = None

  // Сброс редактируемого элемента при перходе по breadcrumbs во время его редактирования
  def resetEditedElements(): Unit = editedIndex = None
}
