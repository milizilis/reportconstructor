package ReportConstructor

import ReportConstructor.UI.Pages.MainPage

import scala.scalajs.js.JSApp

/**
  * Created by Denis on 17.03.2017.
  */

object ReportConstructorLauncher extends JSApp {
  def main(): Unit = Utils.RedirectTo(MainPage)
}