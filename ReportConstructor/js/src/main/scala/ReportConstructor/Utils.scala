package ReportConstructor

import ReportConstructor.Model.IndexInfo
import ReportConstructor.UI.Pages.BreadPage
import org.scalajs.dom.DragEvent
import org.scalajs.dom.html.{Div, Input, LI}
import org.scalajs.jquery.jQuery

import scala.scalajs.js
import scalatags.JsDom.TypedTag
import scalatags.JsDom.all._
import scala.scalajs.concurrent.JSExecutionContext.Implicits.runNow

/**
  * Created by Denis on 10.04.2017.
  */

object Utils {
  /** ***************************************** TagDivFormulaConversion *****************************************/
  def getFormulaFromDiv(divId: String): String = jQuery("#" + divId).html().filter(_ >= ' ').replaceAll("<.*?id=\"", "{").replaceAll("\".*?>", "}")

  def setDivFromFormula(formula: String, divId: String): Unit = {
    val pattern = "{.*?}".r

    jQuery("#" + divId).html(pattern.replaceAllIn(formula, key => {
      var replacer = ""
      Local.getIndexByKey(key.toString().replaceAll("{|}", "")).map(index => replacer = getTagByIndex(index).toString)
      replacer
    }))
  }

  def getTagByIndex(index: Option[IndexInfo]): TypedTag[Input] = {
    input(
      `type` := "button",
      `class` := "divIndexIcon",
      borderColor := {
        index.fold("black")(_.groupKey match {
          case NoDBIndexGroupKeys.ig_Usr => "orange"
          case primaryIndicesGroupKey =>
            var borderColor = ""
            Local.getIndexGroupByKey(primaryIndicesGroupKey).map(indexGroup => borderColor = indexGroup.fold("black")(_.iconDetails.borderColor))
            borderColor
        })
      },
      id := index.fold("")(_.key),
      value := index.fold("Ошибка")(_.shortName),
      title := index.fold("Показатель не существует")(_.fullName)
    )
  }

  /** ***************************************** drag'n'drop *****************************************/
  object draggableIndexInfo {
    var index: Option[IndexInfo] = None
  }

  def drag(ev: DragEvent): Unit = Local.getIndexByKey(jQuery(ev.target).attr("id").toString).map(draggableIndexInfo.index = _)

  def drop(ev: DragEvent, droppableDivId: String, allowRepeatedDrop: Boolean = true): Unit = {
    ev.preventDefault()

    def dropIndex = jQuery("#" + droppableDivId).append(getTagByIndex(draggableIndexInfo.index).render)

    Local.editedIndex.fold[Unit](dropIndex)(editedIndex => {
      draggableIndexInfo.index.map(_.key).foreach(key => {
        if (key == editedIndex.key) js.Dynamic.global.alert("Рекурентная вставка невозможна") else dropIndex
      })
    })

    draggableIndexInfo.index = None
  }

  def allowDrop(ev: DragEvent): Unit = ev.preventDefault()

  /** ***************************************** navigation *****************************************/
  def RedirectTo(page: BreadPage): Unit = {

    def setPath(page: BreadPage): Unit = {
      val breadcrumbs = jQuery("#olNavigation").empty()
      getNavList(Some(page), Nil).foreach(li => breadcrumbs.append(li.render))
      jQuery("#" + page.getNavNodeId).empty().addClass("active").html(page.getName) // Делаем текущую страницу активной
    }

    def getNavList(page: Option[BreadPage], list: List[TypedTag[LI]]): List[TypedTag[LI]] = page.fold(list)(p => getNavList(p.getParent, p.navigationNode :: list))

    def setContent(contentDiv: TypedTag[Div]): Unit = jQuery("#divContainer").empty().append(contentDiv.render)

    setPath(page)
    setContent(page.content)
    page.initialize()
  }
}
