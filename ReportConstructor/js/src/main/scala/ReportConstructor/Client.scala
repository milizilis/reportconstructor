package ReportConstructor

import org.scalajs.dom
import org.scalajs.dom.raw.XMLHttpRequest
import upickle.Js
import upickle.default._

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.runNow
import scala.scalajs.js

/**
  * Created by Denis on 23.04.2017.
  */

object Client extends autowire.Client[Js.Value, Reader, Writer] {
  override def doCall(req: Request): Future[Js.Value] = {
    dom.ext.Ajax.post(
      url = "api/" + req.path.mkString("/"),
      data = upickle.json.write(Js.Obj(req.args.toSeq: _*))
    ).recover {
      case dom.ext.AjaxException(req) =>
        println(req.responseText)
        req.responseText match {
          case "auth" =>
            val returnUtl = js.Dynamic.global.window.location
            if (js.Dynamic.global.confirm("Вход не выполнен. Перейти на страницу аутентификации?").asInstanceOf[Boolean])
              js.Dynamic.global.window.location = "https://df.asutk.ru/AuthenticationPage?returnUrl=" + returnUtl
          case "rights" =>
            js.Dynamic.global.window.location = "https://df.asutk.ru/noRightsPage"
        }
    }.map(_.asInstanceOf[XMLHttpRequest].responseText)
      .map(upickle.json.read)
  }

  def read[Result: Reader](p: Js.Value) = readJs[Result](p)

  def write[Result: Writer](r: Result) = writeJs(r)
}
