package ReportConstructor

import ReportConstructor.Model._

/**
  * Created by Denis on 23.04.2017.
  */

trait ServerAPI {
  def loadIndexGroups(): List[IndexGroup]

  def loadIndices(): List[IndexInfo]

  def loadRegions(): List[RegionInfo]

  def loadReports(): List[ReportInfo]

  def insertIndex(index: IndexInfo): Unit

  def deleteIndex(key: String): Unit

  def updateIndex(index: IndexInfo): Unit

  def insertReport(report: ReportInfo): Unit

  def updateReport(report: ReportInfo): Unit

  def deleteReport(key: String): Unit

  def getReportSeries(reportDetails: reportDetails): List[SeriesElement]
}
