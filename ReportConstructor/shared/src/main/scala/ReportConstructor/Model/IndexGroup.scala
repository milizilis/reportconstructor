package ReportConstructor.Model

/**
  * Created by Denis on 17.03.2017.
  */

case class IndexGroup(key: String, shortName: String, fullName: String, iconDetails: IconDetails)
