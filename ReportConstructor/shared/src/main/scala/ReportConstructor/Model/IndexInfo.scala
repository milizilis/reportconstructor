package ReportConstructor.Model

/**
  * Created by Denis on 10.04.2017.
  */
case class IndexInfo(key: String = "",
                     code: String,
                     groupKey: String,
                     shortName: String,
                     fullName: String,
                     parentKey: String,
                     isLeaf: Boolean,
                     isUser: Boolean,
                     formula: String)
