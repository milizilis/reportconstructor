package ReportConstructor.Model

/**
  * Created by Denis on 18.04.2017.
  */

case class SeriesElement(argument: String, value: Double, tag: String)
