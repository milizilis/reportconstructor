package ReportConstructor.Model

/**
  * Created by Denis on 10.04.2017.
  */

case class ReportInfo(key: String, reportName: String, details: reportDetails)

case class reportDetails(indexKeysList: List[String],
                         regionKeysList: List[String],
                         rangeFromYQ: (Int, Int),
                         rangeToYQ: (Int, Int),
                         diagramType: String,
                         considerQuarters: Boolean)