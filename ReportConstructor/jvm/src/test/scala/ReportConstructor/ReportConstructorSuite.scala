package ReportConstructor

import org.scalatest.FunSuite
import scalikejdbc.config.DBs

import scala.util.matching.Regex

/**
  * Created by Denis on 06.06.2017.
  */

class ReportConstructorSuite extends FunSuite {
  test("getPrimaryFormulaTest") {
    val db = new DBLogic

    DBs.setupAll()

    val allIndices = db.loadIndices()

    def getIndexByKey(key: String) = allIndices.find(_.key == key)

    def extractKey(reg: Regex.Match): String = reg.toString.replaceAll("\\{|\\}", "")

    def getPrimaryFormula(formula: String): String =
      if (!"\\{.*?\\}".r.findAllMatchIn(formula).map(m => getIndexByKey(extractKey(m))).exists(_.fold(false)(_.isUser))) formula
      else getPrimaryFormula("\\{.*?\\}".r.replaceAllIn(formula, r => getIndexByKey(extractKey(r)).fold("")(index =>
        if (index.isUser) "(" + index.formula + ")" else r.toString)))

    assert(getPrimaryFormula("{116860346}") == "(((({37341}+{113615547})*2/({113615548}+{113615551}))+{115825582})/({115825587}*100))")
    info("well done!")
  }

}
