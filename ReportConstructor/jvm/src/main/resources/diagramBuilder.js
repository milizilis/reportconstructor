/**
 * Created by Denis on 16.04.2017.
 */

function drawChart(indexDiagramInfo) {
    var diagramInfo = JSON.parse(indexDiagramInfo);

    $("#cbShowDataOnDiagram").dxCheckBox({
        value: false,
        text: "Указывать атрибуты данных на диаграмме",
        onValueChanged: function (e) {
            try {
                var chart = $("#divReportView").dxChart("instance");
                if (!e.value) {
                    chart.option("commonSeriesSettings.label.position", "outside");
                    chart.render();
                    chart.option("commonSeriesSettings.label.rotationAngle", 0);
                }
                else {
                    chart.option("commonSeriesSettings.label.position", "inside");
                    chart.render();
                    chart.option("commonSeriesSettings.label.rotationAngle", 270);
                }
            } catch (e) {

            }
        }
    });

    $("#divReportView").dxChart({
        palette: "Soft Pastel",
        dataSource: diagramInfo.series,
        title: diagramInfo.diagramName,
        commonSeriesSettings: {
            type: diagramInfo.diagramType,
            argumentField: "argument",
            valueField: "value",
            tagField: "tag",
            label: {
                visible: true,
                customizeText: function () {
                    return $("#cbShowDataOnDiagram").dxCheckBox("instance").option("value")
                        ? this.point.tag + ": " + this.value
                        : this.value;
                }
            }
        },
        seriesTemplate: {
            nameField: "tag"
        },
        argumentAxis: {
            type: "discrete"
        },
        "export": {
            enabled: true
        },
        legend: {
            visible: true,
            verticalAlignment: 'bottom',
            horizontalAlignment: 'center'
        }
    });
}