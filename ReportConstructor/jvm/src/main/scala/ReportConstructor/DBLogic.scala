package ReportConstructor

import java.sql.Connection

import ReportConstructor.Model._
import anorm._
import io.circe.generic.auto._
import io.circe.parser.decode
import io.circe.syntax._
import scalikejdbc.DB

import scala.util.matching.Regex

/**
  * Created by Denis on 04.05.2017.
  */

class DBLogic extends ServerAPI {

  object db {
    def run[A](block: Connection => A): A = {
      DB autoCommit {
        implicit session => block(session.connection)
      }
    }
  }

  case class extracted_IndexGroup(key: Long, fullName: String, shortName: String, details: String)

  def loadIndexGroups(): List[IndexGroup] =
    db.run { implicit connection =>
      SQL("SELECT NKEY AS KEY, CNAME AS FULLNAME, CSHORTNAME AS SHORTNAME, CRCDETAILS AS DETAILS FROM RF_EDIT_TYPE WHERE CRCDETAILS IS NOT NULL ORDER BY KEY")
        .as(Macro.namedParser[extracted_IndexGroup].*).map(i =>
        IndexGroup(
          key = i.key.toString,
          shortName = i.shortName,
          fullName = i.fullName,
          iconDetails = decode[IconDetails](i.details) match {
            case Left(_) => IconDetails(borderColor = "black")
            case Right(iconDetails) => iconDetails
          }
        ))
    }

  case class extracted_IndexInfo(key: Long,
                                 code: String,
                                 groupKey: Long,
                                 shortName: String,
                                 fullName: String,
                                 isLeaf: Int,
                                 parentKey: Long,
                                 isUser: Int,
                                 formula: Option[String])

  def loadIndices(): List[IndexInfo] =
    db.run { implicit connection =>
      SQL("SELECT KEY, CODE, GROUPKEY, SHORTNAME, FULLNAME, ISLEAF, PARENTKEY, ISUSER, FORMULA FROM V_RF_RC_INDICES")
        .as(Macro.namedParser[extracted_IndexInfo].*).map(i =>
        IndexInfo(
          key = i.key.toString,
          code = i.code,
          groupKey = i.groupKey.toString,
          shortName = i.shortName,
          fullName = i.fullName,
          isLeaf = i.isLeaf == 1,
          parentKey = i.parentKey.toString,
          isUser = i.isUser == 1,
          formula = i.formula.getOrElse("")
        ))
    }

  case class extracted_RegionInfo(key: Long, name: String)

  def loadRegions(): List[RegionInfo] =
    db.run { implicit connection =>
      SQL("SELECT NKEY AS KEY, REG_REGIONNAME AS NAME FROM RF_REGION WHERE REG_TYPEKEY IN (1, 5)")
        .as(Macro.namedParser[extracted_RegionInfo].*).map(i =>
        RegionInfo(key = i.key.toString, name = i.name))
    }

  case class extracted_ReportInfo(key: Long, name: String, details: String)

  def loadReports(): List[ReportInfo] = {
    db.run { implicit connection =>
      SQL("SELECT KEY, NAME, DETAILS FROM RF_RC_REPORTS")
        .as(Macro.namedParser[extracted_ReportInfo].*).map(i =>
        ReportInfo(
          key = i.key.toString,
          reportName = i.name,
          details = decode[reportDetails](i.details) match {
            case Left(_) => reportDetails(Nil, Nil, 0 -> 0, 0 -> 0, "error", false)
            case Right(details) => details
          }
        )
      )
    }
  }

  def insertIndex(index: IndexInfo): Unit =
    db.run { implicit connection =>
      SQL("INSERT INTO RF_RC_USR_INDICES (KEY, CODE, SHORTNAME, FULLNAME, FORMULA) VALUES (RF_UTIL.GETNEWKEY, {CODE}, {SHORTNAME}, {FULLNAME}, {FORMULA})")
        .on(
          'CODE -> index.code,
          'SHORTNAME -> index.shortName,
          'FULLNAME -> index.fullName,
          'FORMULA -> index.formula)
        .execute()
    }

  def deleteIndex(key: String): Unit =
    db.run { implicit connection =>
      SQL("DELETE FROM RF_RC_USR_INDICES WHERE KEY = {KEY}")
        .on('KEY -> key)
        .executeUpdate()
    }

  def updateIndex(index: IndexInfo): Unit =
    db.run { implicit connection =>
      SQL("UPDATE RF_RC_USR_INDICES SET CODE = {CODE}, SHORTNAME = {SHORTNAME}, FULLNAME = {FULLNAME} WHERE KEY = {KEY}")
        .on(
          'KEY -> index.key,
          'CODE -> index.code,
          'SHORTNAME -> index.shortName,
          'FULLNAME -> index.fullName,
          'FORMULA -> index.formula)
        .executeUpdate()
    }

  def insertReport(report: ReportInfo): Unit =
    db.run { implicit connection =>
      SQL("INSERT INTO RF_RC_REPORTS (KEY, NAME, DETAILS) VALUES (RF_UTIL.GETNEWKEY, {NAME}, {DETAILS})")
        .on(
          'NAME -> report.reportName,
          'DETAILS -> report.details.asJson.noSpaces)
        .execute()
    }

  def updateReport(report: ReportInfo): Unit =
    db.run { implicit connection =>
      SQL("UPDATE RF_RC_REPORTS SET NAME = {NAME}, DETAILS = {DETAILS} WHERE KEY = {KEY}")
        .on(
          'KEY -> report.key,
          'NAME -> report.reportName,
          'DETAILS -> report.details.asJson.noSpaces)
        .executeUpdate()
    }

  def deleteReport(key: String): Unit =
    db.run { implicit connection =>
      SQL("DELETE FROM RF_RC_REPORTS WHERE KEY = {KEY}")
        .on('KEY -> key)
        .executeUpdate()
    }

  case class extracted_SeriesElement(argument: String, value: Option[Double], tag: String)

  def getReportSeries(reportDetails: reportDetails): List[SeriesElement] = {

    val allIndices = loadIndices()

    def getIndexByKey(key: String) = allIndices.find(_.key == key)

    def extractKey(reg: Regex.Match): String = reg.toString.replaceAll("\\{|\\}", "")

    def getPrimaryFormula(formula: String): String =
      if (!"\\{.*?\\}".r.findAllMatchIn(formula).map(m => getIndexByKey(extractKey(m))).exists(_.fold(false)(_.isUser))) formula
      else getPrimaryFormula("\\{.*?\\}".r.replaceAllIn(formula, r => getIndexByKey(extractKey(r)).fold("")(index =>
        if (index.isUser) "(" + index.formula + ")" else r.toString)))

    db.run { implicit connection =>
      var series = List[SeriesElement]()
      reportDetails.indexKeysList.foreach(indexKey => {
        val index = getIndexByKey(indexKey)
        val primaryFormula = index.fold("")(i => getPrimaryFormula(i.formula))

        series = series ::: SQL("SELECT ARGUMENT, VALUE, TAG " +
          "FROM TABLE(RF_RC.GETSERIES(" +
          "{FORMULA}, " +
          "{FORMULAINDEXKEYLIST}, " +
          "{INDEXSHORTNAME}, " +
          "{REGIONKEYLIST}, " +
          "{YEARFROM}, " +
          "{QUARTERFROM}, " +
          "{YEARTO}, " +
          "{QUARTERTO}, " +
          "{CONSIDERQUARTERS}))")
          .on(
            'FORMULA -> primaryFormula,
            'FORMULAINDEXKEYLIST -> "\\{.*?\\}".r.findAllMatchIn(primaryFormula).map(_.toString.replaceAll("\\{|\\}", "")).mkString(","),
            'INDEXSHORTNAME -> index.fold("")(_.shortName),
            'REGIONKEYLIST -> reportDetails.regionKeysList.mkString(","),
            'YEARFROM -> reportDetails.rangeFromYQ._1,
            'QUARTERFROM -> reportDetails.rangeFromYQ._2,
            'YEARTO -> reportDetails.rangeToYQ._1,
            'QUARTERTO -> reportDetails.rangeToYQ._2,
            'CONSIDERQUARTERS -> {
              if (reportDetails.considerQuarters) 1 else 0
            }
          )
          .as(Macro.namedParser[extracted_SeriesElement].*)
          .map(series => SeriesElement(argument = series.argument, value = series.value.getOrElse(0), tag = series.tag))
      })
      series
    }
  }
}
