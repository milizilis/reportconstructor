package ReportConstructor

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.server.Directives._
import akka.stream.ActorMaterializer
import scalikejdbc.config._
import upickle.Js
import upickle.default._

import scala.concurrent.ExecutionContext.Implicits.global

object AutowireServer extends autowire.Server[Js.Value, Reader, Writer] {
  def read[Result: Reader](p: Js.Value) = upickle.default.readJs[Result](p)

  def write[Result: Writer](r: Result) = upickle.default.writeJs(r)
}

object StaticContent {

  import scalatags.Text.all._
  import scalatags.Text.tags2.title

  val document: String =
    "<!DOCTYPE html>" +
      html(
        head(
          title("ReportConstructor"),
          meta(httpEquiv := "Content-Type", content := "text/html; charset=UTF-8"),
          link(rel := "stylesheet", href := "bootstrap-3.3.7-dist/css/bootstrap.css"),
          link(rel := "stylesheet", href := "styles.css", `type` := "text/css"),
          script(`type` := "text/javascript", src := "https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"),
          script(`type` := "text/javascript", src := "bootstrap-3.3.7-dist/js/bootstrap.js"),
          script(`type` := "text/javascript", src := "diagramBuilder.js"),

          script(`type` := "text/javascript", src := "DevExtreme/Scripts/dx.all.js"),
          link(rel := "stylesheet", `type` := "text/css", href := "DevExtreme/Content/dx.common.css"),
          link(rel := "stylesheet", `type` := "text/css", href := "DevExtreme/Content/dx.light.css")
        ),
        body(margin := 0)(
          ol(id := "olNavigation", `class` := "breadcrumb"),
          div(id := "divContainer"),

          script(`type` := "text/javascript", src := "client-fastopt.js"),
          script(`type` := "text/javascript", src := "client-launcher.js"),
          script(`type` := "text/javascript", src := "//localhost:12345/workbench.js")
        )
      ).render
}

object Server extends DBLogic with App {
  implicit val system = ActorSystem()
  implicit val materializer = ActorMaterializer()
  val route = {
    get {
      pathSingleSlash {
        complete {
          HttpEntity(ContentTypes.`text/html(UTF-8)`, StaticContent.document)
        }
      } ~
        getFromResourceDirectory("")
    } ~
      post {
        path("api" / Segments) { s =>
          extract(rc => {
            val cookieSec = rc.request.headers.collect { case `Cookie`(x) ⇒ x }
            rc.request.entity match {
              case HttpEntity.Strict(nb: ContentType.NonBinary, data) =>
                (data.decodeString(nb.charset.value), cookieSec)
            }
          }) { case (e, cookieSec) =>
            complete {
              /*var isAuthorized = true

              val hasRightsSecFuture = cookieSec.map(cookies => {
                val checkRightsRequest = HttpRequest(
                  method = HttpMethods.POST,
                  uri = "http://localhost:22238/Content/Forms/RCCheckRights",
                  headers = Seq[HttpHeader](Cookie(cookies))
                )
                for {
                  checkRightsResponse <- Http().singleRequest(checkRightsRequest)
                  _ = if (!checkRightsResponse.status.isSuccess()) checkRightsResponse.discardEntityBytes()
                  if checkRightsResponse.status.isSuccess()
                  result <- Unmarshal(checkRightsResponse).to[String]
                } yield Try(result.toBoolean).getOrElse({
                  isAuthorized = false
                  false
                })
              })

              val hasRights = Await.result(Future.sequence(hasRightsSecFuture).map(_.reduce(_ && _)), 10 seconds)

              if (!isAuthorized) HttpResponse(status = InternalServerError, entity = "auth")
              else if (isAuthorized && !hasRights) HttpResponse(status = InternalServerError, entity = "rights")
              else*/
                AutowireServer.route[ServerAPI](Server)(
                  autowire.Core.Request(s, upickle.json.read(e).asInstanceOf[Js.Obj].value.toMap)
                ).map(upickle.json.write(_))
            }
          }
        }
      }
  }

  Http().bindAndHandle(route, "0.0.0.0", port = 8081)
  DBs.setupAll()
}