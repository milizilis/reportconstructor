Команда запуска: 

```
sbt ~re-start
```

Конструктор отчетов работает на `localhost:8081`

**Комментарии по разработке**

Для использования Autowire добавить в import:

```
import autowire._
import scala.scalajs.concurrent.JSExecutionContext.Implicits.runNow
```

Для работы с проверкой прав доступа (Server.scala) добавить в import:

```
import akka.http.scaladsl.unmarshalling.Unmarshal
import scala.collection.immutable.Seq
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.Try
import StatusCodes._
```

Возможное лечение ошибки _GC Overhead limit exceeded_:

```
sbt clean
sbt ~re-start
```